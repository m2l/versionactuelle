﻿namespace MaisonDesLigues
{
    partial class FrmPrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipale));
            this.TabInscription = new System.Windows.Forms.TabPage();
            this.GrpBenevole = new System.Windows.Forms.GroupBox();
            this.BtnEnregistreBenevole = new System.Windows.Forms.Button();
            this.PanelDispoBenevole = new System.Windows.Forms.Panel();
            this.TxtLicenceBenevole = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtDateNaissance = new System.Windows.Forms.MaskedTextBox();
            this.CmdQuitter = new System.Windows.Forms.Button();
            this.GrpTypeParticipant = new System.Windows.Forms.GroupBox();
            this.RadLicencie = new System.Windows.Forms.RadioButton();
            this.RadBenevole = new System.Windows.Forms.RadioButton();
            this.RadIntervenant = new System.Windows.Forms.RadioButton();
            this.GrpIdentite = new System.Windows.Forms.GroupBox();
            this.TxtMail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.MaskedTextBox();
            this.TxtCp = new System.Windows.Forms.MaskedTextBox();
            this.TxtVille = new System.Windows.Forms.TextBox();
            this.TxtAdr2 = new System.Windows.Forms.TextBox();
            this.TxtAdr1 = new System.Windows.Forms.TextBox();
            this.TxtPrenom = new System.Windows.Forms.TextBox();
            this.TxtNom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PicAffiche = new System.Windows.Forms.PictureBox();
            this.GrpIntervenant = new System.Windows.Forms.GroupBox();
            this.BtnEnregistrerIntervenant = new System.Windows.Forms.Button();
            this.GrpNuiteIntervenant = new System.Windows.Forms.GroupBox();
            this.PanNuiteIntervenant = new System.Windows.Forms.Panel();
            this.RdbNuiteIntervenantNon = new System.Windows.Forms.RadioButton();
            this.RdbNuiteIntervenantOui = new System.Windows.Forms.RadioButton();
            this.PanFonctionIntervenant = new System.Windows.Forms.Panel();
            this.CmbAtelierIntervenant = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TabPrincipal = new System.Windows.Forms.TabControl();
            this.TabCreer = new System.Windows.Forms.TabPage();
            this.BtnEnregistrerAjout = new System.Windows.Forms.Button();
            this.GrpBVacationAjout = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LblDateDeFin = new System.Windows.Forms.Label();
            this.LblDateDeDebut = new System.Windows.Forms.Label();
            this.dateTimePickerHeureFin = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerHeureDebut = new System.Windows.Forms.DateTimePicker();
            this.TxtBoxNumeroVacation = new System.Windows.Forms.TextBox();
            this.LblNumeroVacation = new System.Windows.Forms.Label();
            this.LblAtelierVacation = new System.Windows.Forms.Label();
            this.CmbAtelierVacation = new System.Windows.Forms.ComboBox();
            this.GrpBThemeAjout = new System.Windows.Forms.GroupBox();
            this.CmbAtelierTheme = new System.Windows.Forms.ComboBox();
            this.TxtBoxLibelleTheme = new System.Windows.Forms.TextBox();
            this.TxtBoxNumeroTheme = new System.Windows.Forms.TextBox();
            this.LblLibelleTheme = new System.Windows.Forms.Label();
            this.LblNumeroTheme = new System.Windows.Forms.Label();
            this.LblAtelierTheme = new System.Windows.Forms.Label();
            this.GrpBAtelierAjout = new System.Windows.Forms.GroupBox();
            this.TxtBoxLibelleAtelier = new System.Windows.Forms.TextBox();
            this.NmUDPlacesMaxi = new System.Windows.Forms.NumericUpDown();
            this.LblNbPlacesMaxi = new System.Windows.Forms.Label();
            this.LblLibelleAtelier = new System.Windows.Forms.Label();
            this.GrpBChoixAjout = new System.Windows.Forms.GroupBox();
            this.RdbVacationAjout = new System.Windows.Forms.RadioButton();
            this.RdbThemeAjout = new System.Windows.Forms.RadioButton();
            this.RdbAtelierAjout = new System.Windows.Forms.RadioButton();
            this.TabModifier = new System.Windows.Forms.TabPage();
            this.GrpBVacationModifier = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePickerHeureFinModifier = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerHeureDebutModifier = new System.Windows.Forms.DateTimePicker();
            this.LblChoixVacation = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RdbVacationModifier = new System.Windows.Forms.RadioButton();
            this.RdbThemeModifier = new System.Windows.Forms.RadioButton();
            this.RdbAtelierModifier = new System.Windows.Forms.RadioButton();
            this.BtnEnregistrerModifier = new System.Windows.Forms.Button();
            this.GrpBThemeModifier = new System.Windows.Forms.GroupBox();
            this.LblChoixTheme = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.CmbAtelierThemeModifier = new System.Windows.Forms.ComboBox();
            this.TxtBoxLibelleThemeModifier = new System.Windows.Forms.TextBox();
            this.TxtBoxNumeroThemeModifier = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.GrpBAtelierModifier = new System.Windows.Forms.GroupBox();
            this.LblChoixAtelier = new System.Windows.Forms.Label();
            this.CmbChoixAtelier = new System.Windows.Forms.ComboBox();
            this.TxtBoxLibelleAtelierModifier = new System.Windows.Forms.TextBox();
            this.NmUDPlacesMaxiModifier = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TabInscription.SuspendLayout();
            this.GrpBenevole.SuspendLayout();
            this.GrpTypeParticipant.SuspendLayout();
            this.GrpIdentite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicAffiche)).BeginInit();
            this.GrpIntervenant.SuspendLayout();
            this.GrpNuiteIntervenant.SuspendLayout();
            this.TabPrincipal.SuspendLayout();
            this.TabCreer.SuspendLayout();
            this.GrpBVacationAjout.SuspendLayout();
            this.GrpBThemeAjout.SuspendLayout();
            this.GrpBAtelierAjout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NmUDPlacesMaxi)).BeginInit();
            this.GrpBChoixAjout.SuspendLayout();
            this.TabModifier.SuspendLayout();
            this.GrpBVacationModifier.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GrpBThemeModifier.SuspendLayout();
            this.GrpBAtelierModifier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NmUDPlacesMaxiModifier)).BeginInit();
            this.SuspendLayout();
            // 
            // TabInscription
            // 
            this.TabInscription.Controls.Add(this.GrpBenevole);
            this.TabInscription.Controls.Add(this.CmdQuitter);
            this.TabInscription.Controls.Add(this.GrpTypeParticipant);
            this.TabInscription.Controls.Add(this.GrpIdentite);
            this.TabInscription.Controls.Add(this.PicAffiche);
            this.TabInscription.Controls.Add(this.GrpIntervenant);
            this.TabInscription.Location = new System.Drawing.Point(4, 22);
            this.TabInscription.Name = "TabInscription";
            this.TabInscription.Padding = new System.Windows.Forms.Padding(3);
            this.TabInscription.Size = new System.Drawing.Size(956, 579);
            this.TabInscription.TabIndex = 0;
            this.TabInscription.Text = "Inscription";
            this.TabInscription.UseVisualStyleBackColor = true;
            // 
            // GrpBenevole
            // 
            this.GrpBenevole.Controls.Add(this.BtnEnregistreBenevole);
            this.GrpBenevole.Controls.Add(this.PanelDispoBenevole);
            this.GrpBenevole.Controls.Add(this.TxtLicenceBenevole);
            this.GrpBenevole.Controls.Add(this.label9);
            this.GrpBenevole.Controls.Add(this.label8);
            this.GrpBenevole.Controls.Add(this.TxtDateNaissance);
            this.GrpBenevole.Location = new System.Drawing.Point(607, 23);
            this.GrpBenevole.Name = "GrpBenevole";
            this.GrpBenevole.Size = new System.Drawing.Size(564, 174);
            this.GrpBenevole.TabIndex = 23;
            this.GrpBenevole.TabStop = false;
            this.GrpBenevole.Text = "Disponibilités Bénévole";
            this.GrpBenevole.Visible = false;
            // 
            // BtnEnregistreBenevole
            // 
            this.BtnEnregistreBenevole.Enabled = false;
            this.BtnEnregistreBenevole.Location = new System.Drawing.Point(321, 143);
            this.BtnEnregistreBenevole.Name = "BtnEnregistreBenevole";
            this.BtnEnregistreBenevole.Size = new System.Drawing.Size(133, 25);
            this.BtnEnregistreBenevole.TabIndex = 1;
            this.BtnEnregistreBenevole.Text = "Enregistrer";
            this.BtnEnregistreBenevole.UseVisualStyleBackColor = true;
            this.BtnEnregistreBenevole.Click += new System.EventHandler(this.BtnEnregistreBenevole_Click);
            // 
            // PanelDispoBenevole
            // 
            this.PanelDispoBenevole.Location = new System.Drawing.Point(28, 84);
            this.PanelDispoBenevole.Name = "PanelDispoBenevole";
            this.PanelDispoBenevole.Size = new System.Drawing.Size(251, 84);
            this.PanelDispoBenevole.TabIndex = 21;
            // 
            // TxtLicenceBenevole
            // 
            this.TxtLicenceBenevole.Location = new System.Drawing.Point(136, 58);
            this.TxtLicenceBenevole.Mask = "000000000000";
            this.TxtLicenceBenevole.Name = "TxtLicenceBenevole";
            this.TxtLicenceBenevole.Size = new System.Drawing.Size(147, 20);
            this.TxtLicenceBenevole.TabIndex = 19;
            this.TxtLicenceBenevole.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkDateBenevole_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Date de naissance : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Numéro de licence : ";
            // 
            // TxtDateNaissance
            // 
            this.TxtDateNaissance.Location = new System.Drawing.Point(136, 32);
            this.TxtDateNaissance.Mask = "00/00/0000";
            this.TxtDateNaissance.Name = "TxtDateNaissance";
            this.TxtDateNaissance.Size = new System.Drawing.Size(147, 20);
            this.TxtDateNaissance.TabIndex = 2;
            this.TxtDateNaissance.Text = "15081985";
            this.TxtDateNaissance.ValidatingType = typeof(System.DateTime);
            this.TxtDateNaissance.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChkDateBenevole_CheckedChanged);
            // 
            // CmdQuitter
            // 
            this.CmdQuitter.Location = new System.Drawing.Point(493, 180);
            this.CmdQuitter.Name = "CmdQuitter";
            this.CmdQuitter.Size = new System.Drawing.Size(144, 36);
            this.CmdQuitter.TabIndex = 22;
            this.CmdQuitter.Text = "Quitter";
            this.CmdQuitter.UseVisualStyleBackColor = true;
            this.CmdQuitter.Click += new System.EventHandler(this.CmdQuitter_Click);
            // 
            // GrpTypeParticipant
            // 
            this.GrpTypeParticipant.Controls.Add(this.RadLicencie);
            this.GrpTypeParticipant.Controls.Add(this.RadBenevole);
            this.GrpTypeParticipant.Controls.Add(this.RadIntervenant);
            this.GrpTypeParticipant.Location = new System.Drawing.Point(23, 6);
            this.GrpTypeParticipant.Name = "GrpTypeParticipant";
            this.GrpTypeParticipant.Size = new System.Drawing.Size(453, 58);
            this.GrpTypeParticipant.TabIndex = 21;
            this.GrpTypeParticipant.TabStop = false;
            this.GrpTypeParticipant.Text = "Type Participant";
            // 
            // RadLicencie
            // 
            this.RadLicencie.AutoSize = true;
            this.RadLicencie.Location = new System.Drawing.Point(195, 19);
            this.RadLicencie.Name = "RadLicencie";
            this.RadLicencie.Size = new System.Drawing.Size(65, 17);
            this.RadLicencie.TabIndex = 20;
            this.RadLicencie.TabStop = true;
            this.RadLicencie.Text = "Licencié";
            this.RadLicencie.UseVisualStyleBackColor = true;
            this.RadLicencie.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // RadBenevole
            // 
            this.RadBenevole.AutoSize = true;
            this.RadBenevole.Location = new System.Drawing.Point(351, 19);
            this.RadBenevole.Name = "RadBenevole";
            this.RadBenevole.Size = new System.Drawing.Size(70, 17);
            this.RadBenevole.TabIndex = 19;
            this.RadBenevole.TabStop = true;
            this.RadBenevole.Text = "Bénévole";
            this.RadBenevole.UseVisualStyleBackColor = true;
            this.RadBenevole.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // RadIntervenant
            // 
            this.RadIntervenant.AutoSize = true;
            this.RadIntervenant.Location = new System.Drawing.Point(44, 19);
            this.RadIntervenant.Name = "RadIntervenant";
            this.RadIntervenant.Size = new System.Drawing.Size(79, 17);
            this.RadIntervenant.TabIndex = 18;
            this.RadIntervenant.TabStop = true;
            this.RadIntervenant.Text = "Intervenant";
            this.RadIntervenant.UseVisualStyleBackColor = true;
            this.RadIntervenant.CheckedChanged += new System.EventHandler(this.RadTypeParticipant_Changed);
            // 
            // GrpIdentite
            // 
            this.GrpIdentite.Controls.Add(this.TxtMail);
            this.GrpIdentite.Controls.Add(this.label7);
            this.GrpIdentite.Controls.Add(this.label6);
            this.GrpIdentite.Controls.Add(this.txtTel);
            this.GrpIdentite.Controls.Add(this.TxtCp);
            this.GrpIdentite.Controls.Add(this.TxtVille);
            this.GrpIdentite.Controls.Add(this.TxtAdr2);
            this.GrpIdentite.Controls.Add(this.TxtAdr1);
            this.GrpIdentite.Controls.Add(this.TxtPrenom);
            this.GrpIdentite.Controls.Add(this.TxtNom);
            this.GrpIdentite.Controls.Add(this.label5);
            this.GrpIdentite.Controls.Add(this.label4);
            this.GrpIdentite.Controls.Add(this.label3);
            this.GrpIdentite.Controls.Add(this.label2);
            this.GrpIdentite.Controls.Add(this.label1);
            this.GrpIdentite.Location = new System.Drawing.Point(23, 70);
            this.GrpIdentite.Name = "GrpIdentite";
            this.GrpIdentite.Size = new System.Drawing.Size(453, 188);
            this.GrpIdentite.TabIndex = 17;
            this.GrpIdentite.TabStop = false;
            this.GrpIdentite.Text = "Identité";
            // 
            // TxtMail
            // 
            this.TxtMail.Location = new System.Drawing.Point(232, 148);
            this.TxtMail.Name = "TxtMail";
            this.TxtMail.Size = new System.Drawing.Size(189, 20);
            this.TxtMail.TabIndex = 16;
            this.TxtMail.Text = "patrick.dumoulin@toto.fr";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(188, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Mail :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Tél :";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(77, 148);
            this.txtTel.Mask = "00 00 00 00 00 00";
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(100, 20);
            this.txtTel.TabIndex = 13;
            // 
            // TxtCp
            // 
            this.TxtCp.Location = new System.Drawing.Point(87, 107);
            this.TxtCp.Mask = "00000";
            this.TxtCp.Name = "TxtCp";
            this.TxtCp.Size = new System.Drawing.Size(90, 20);
            this.TxtCp.TabIndex = 12;
            this.TxtCp.Text = "75001";
            // 
            // TxtVille
            // 
            this.TxtVille.Location = new System.Drawing.Point(232, 104);
            this.TxtVille.Name = "TxtVille";
            this.TxtVille.Size = new System.Drawing.Size(189, 20);
            this.TxtVille.TabIndex = 11;
            this.TxtVille.Text = "Paris";
            // 
            // TxtAdr2
            // 
            this.TxtAdr2.Location = new System.Drawing.Point(81, 76);
            this.TxtAdr2.Name = "TxtAdr2";
            this.TxtAdr2.Size = new System.Drawing.Size(340, 20);
            this.TxtAdr2.TabIndex = 9;
            // 
            // TxtAdr1
            // 
            this.TxtAdr1.Location = new System.Drawing.Point(81, 50);
            this.TxtAdr1.Name = "TxtAdr1";
            this.TxtAdr1.Size = new System.Drawing.Size(340, 20);
            this.TxtAdr1.TabIndex = 8;
            this.TxtAdr1.Text = "Avenue du PLSQL";
            // 
            // TxtPrenom
            // 
            this.TxtPrenom.Location = new System.Drawing.Point(279, 25);
            this.TxtPrenom.Name = "TxtPrenom";
            this.TxtPrenom.Size = new System.Drawing.Size(142, 20);
            this.TxtPrenom.TabIndex = 7;
            this.TxtPrenom.Text = "Patrick";
            // 
            // TxtNom
            // 
            this.TxtNom.Location = new System.Drawing.Point(81, 25);
            this.TxtNom.Name = "TxtNom";
            this.TxtNom.Size = new System.Drawing.Size(142, 20);
            this.TxtNom.TabIndex = 6;
            this.TxtNom.Text = "Dumoulin";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(191, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ville : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CP : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adresse";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prénom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom : ";
            // 
            // PicAffiche
            // 
            this.PicAffiche.Image = ((System.Drawing.Image)(resources.GetObject("PicAffiche.Image")));
            this.PicAffiche.Location = new System.Drawing.Point(493, 6);
            this.PicAffiche.Name = "PicAffiche";
            this.PicAffiche.Size = new System.Drawing.Size(108, 164);
            this.PicAffiche.TabIndex = 5;
            this.PicAffiche.TabStop = false;
            // 
            // GrpIntervenant
            // 
            this.GrpIntervenant.Controls.Add(this.BtnEnregistrerIntervenant);
            this.GrpIntervenant.Controls.Add(this.GrpNuiteIntervenant);
            this.GrpIntervenant.Controls.Add(this.PanFonctionIntervenant);
            this.GrpIntervenant.Controls.Add(this.CmbAtelierIntervenant);
            this.GrpIntervenant.Controls.Add(this.label17);
            this.GrpIntervenant.Location = new System.Drawing.Point(23, 264);
            this.GrpIntervenant.Name = "GrpIntervenant";
            this.GrpIntervenant.Size = new System.Drawing.Size(515, 286);
            this.GrpIntervenant.TabIndex = 25;
            this.GrpIntervenant.TabStop = false;
            this.GrpIntervenant.Text = "Complément Inscription Intervenant";
            this.GrpIntervenant.Visible = false;
            // 
            // BtnEnregistrerIntervenant
            // 
            this.BtnEnregistrerIntervenant.Enabled = false;
            this.BtnEnregistrerIntervenant.Location = new System.Drawing.Point(340, 251);
            this.BtnEnregistrerIntervenant.Name = "BtnEnregistrerIntervenant";
            this.BtnEnregistrerIntervenant.Size = new System.Drawing.Size(133, 25);
            this.BtnEnregistrerIntervenant.TabIndex = 30;
            this.BtnEnregistrerIntervenant.Text = "Enregistrer";
            this.BtnEnregistrerIntervenant.UseVisualStyleBackColor = true;
            this.BtnEnregistrerIntervenant.Click += new System.EventHandler(this.BtnEnregistrerIntervenant_Click);
            // 
            // GrpNuiteIntervenant
            // 
            this.GrpNuiteIntervenant.Controls.Add(this.PanNuiteIntervenant);
            this.GrpNuiteIntervenant.Controls.Add(this.RdbNuiteIntervenantNon);
            this.GrpNuiteIntervenant.Controls.Add(this.RdbNuiteIntervenantOui);
            this.GrpNuiteIntervenant.Location = new System.Drawing.Point(21, 104);
            this.GrpNuiteIntervenant.Name = "GrpNuiteIntervenant";
            this.GrpNuiteIntervenant.Size = new System.Drawing.Size(497, 151);
            this.GrpNuiteIntervenant.TabIndex = 29;
            this.GrpNuiteIntervenant.TabStop = false;
            this.GrpNuiteIntervenant.Text = "Nuités";
            // 
            // PanNuiteIntervenant
            // 
            this.PanNuiteIntervenant.Location = new System.Drawing.Point(3, 43);
            this.PanNuiteIntervenant.Name = "PanNuiteIntervenant";
            this.PanNuiteIntervenant.Size = new System.Drawing.Size(494, 102);
            this.PanNuiteIntervenant.TabIndex = 24;
            this.PanNuiteIntervenant.Visible = false;
            // 
            // RdbNuiteIntervenantNon
            // 
            this.RdbNuiteIntervenantNon.AutoSize = true;
            this.RdbNuiteIntervenantNon.Checked = true;
            this.RdbNuiteIntervenantNon.Location = new System.Drawing.Point(92, 15);
            this.RdbNuiteIntervenantNon.Name = "RdbNuiteIntervenantNon";
            this.RdbNuiteIntervenantNon.Size = new System.Drawing.Size(45, 17);
            this.RdbNuiteIntervenantNon.TabIndex = 23;
            this.RdbNuiteIntervenantNon.TabStop = true;
            this.RdbNuiteIntervenantNon.Text = "Non";
            this.RdbNuiteIntervenantNon.UseVisualStyleBackColor = true;
            this.RdbNuiteIntervenantNon.CheckedChanged += new System.EventHandler(this.RdbNuiteIntervenant_CheckedChanged);
            // 
            // RdbNuiteIntervenantOui
            // 
            this.RdbNuiteIntervenantOui.AutoSize = true;
            this.RdbNuiteIntervenantOui.Location = new System.Drawing.Point(23, 16);
            this.RdbNuiteIntervenantOui.Name = "RdbNuiteIntervenantOui";
            this.RdbNuiteIntervenantOui.Size = new System.Drawing.Size(41, 17);
            this.RdbNuiteIntervenantOui.TabIndex = 22;
            this.RdbNuiteIntervenantOui.Text = "Oui";
            this.RdbNuiteIntervenantOui.UseVisualStyleBackColor = true;
            this.RdbNuiteIntervenantOui.CheckedChanged += new System.EventHandler(this.RdbNuiteIntervenant_CheckedChanged);
            // 
            // PanFonctionIntervenant
            // 
            this.PanFonctionIntervenant.Location = new System.Drawing.Point(305, 4);
            this.PanFonctionIntervenant.Name = "PanFonctionIntervenant";
            this.PanFonctionIntervenant.Size = new System.Drawing.Size(168, 41);
            this.PanFonctionIntervenant.TabIndex = 28;
            // 
            // CmbAtelierIntervenant
            // 
            this.CmbAtelierIntervenant.FormattingEnabled = true;
            this.CmbAtelierIntervenant.Location = new System.Drawing.Point(81, 36);
            this.CmbAtelierIntervenant.Name = "CmbAtelierIntervenant";
            this.CmbAtelierIntervenant.Size = new System.Drawing.Size(218, 21);
            this.CmbAtelierIntervenant.TabIndex = 26;
            this.CmbAtelierIntervenant.TextChanged += new System.EventHandler(this.CmbAtelierIntervenant_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Atelier : ";
            // 
            // TabPrincipal
            // 
            this.TabPrincipal.Controls.Add(this.TabInscription);
            this.TabPrincipal.Controls.Add(this.TabCreer);
            this.TabPrincipal.Controls.Add(this.TabModifier);
            this.TabPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabPrincipal.Location = new System.Drawing.Point(0, 0);
            this.TabPrincipal.Name = "TabPrincipal";
            this.TabPrincipal.SelectedIndex = 0;
            this.TabPrincipal.Size = new System.Drawing.Size(964, 605);
            this.TabPrincipal.TabIndex = 0;
            // 
            // TabCreer
            // 
            this.TabCreer.Controls.Add(this.BtnEnregistrerAjout);
            this.TabCreer.Controls.Add(this.GrpBVacationAjout);
            this.TabCreer.Controls.Add(this.GrpBThemeAjout);
            this.TabCreer.Controls.Add(this.GrpBAtelierAjout);
            this.TabCreer.Controls.Add(this.GrpBChoixAjout);
            this.TabCreer.Location = new System.Drawing.Point(4, 22);
            this.TabCreer.Name = "TabCreer";
            this.TabCreer.Padding = new System.Windows.Forms.Padding(3);
            this.TabCreer.Size = new System.Drawing.Size(956, 579);
            this.TabCreer.TabIndex = 1;
            this.TabCreer.Text = "Créer";
            this.TabCreer.UseVisualStyleBackColor = true;
            // 
            // BtnEnregistrerAjout
            // 
            this.BtnEnregistrerAjout.Location = new System.Drawing.Point(429, 534);
            this.BtnEnregistrerAjout.Name = "BtnEnregistrerAjout";
            this.BtnEnregistrerAjout.Size = new System.Drawing.Size(75, 23);
            this.BtnEnregistrerAjout.TabIndex = 4;
            this.BtnEnregistrerAjout.Text = "Enregistrer";
            this.BtnEnregistrerAjout.UseVisualStyleBackColor = true;
            this.BtnEnregistrerAjout.Click += new System.EventHandler(this.BtnEnregistrerAjout_Click);
            // 
            // GrpBVacationAjout
            // 
            this.GrpBVacationAjout.Controls.Add(this.label12);
            this.GrpBVacationAjout.Controls.Add(this.LblDateDeFin);
            this.GrpBVacationAjout.Controls.Add(this.LblDateDeDebut);
            this.GrpBVacationAjout.Controls.Add(this.dateTimePickerHeureFin);
            this.GrpBVacationAjout.Controls.Add(this.dateTimePickerHeureDebut);
            this.GrpBVacationAjout.Controls.Add(this.TxtBoxNumeroVacation);
            this.GrpBVacationAjout.Controls.Add(this.LblNumeroVacation);
            this.GrpBVacationAjout.Controls.Add(this.LblAtelierVacation);
            this.GrpBVacationAjout.Controls.Add(this.CmbAtelierVacation);
            this.GrpBVacationAjout.Location = new System.Drawing.Point(287, 298);
            this.GrpBVacationAjout.Name = "GrpBVacationAjout";
            this.GrpBVacationAjout.Size = new System.Drawing.Size(364, 201);
            this.GrpBVacationAjout.TabIndex = 3;
            this.GrpBVacationAjout.TabStop = false;
            this.GrpBVacationAjout.Text = "Vacation :";
            this.GrpBVacationAjout.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Crimson;
            this.label12.Location = new System.Drawing.Point(2, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(362, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Attention : Seul les dates et heures seront modifiables après enregistrement!";
            // 
            // LblDateDeFin
            // 
            this.LblDateDeFin.AutoSize = true;
            this.LblDateDeFin.Location = new System.Drawing.Point(41, 171);
            this.LblDateDeFin.Name = "LblDateDeFin";
            this.LblDateDeFin.Size = new System.Drawing.Size(65, 13);
            this.LblDateDeFin.TabIndex = 35;
            this.LblDateDeFin.Text = "Date de fin :";
            // 
            // LblDateDeDebut
            // 
            this.LblDateDeDebut.AutoSize = true;
            this.LblDateDeDebut.Location = new System.Drawing.Point(25, 135);
            this.LblDateDeDebut.Name = "LblDateDeDebut";
            this.LblDateDeDebut.Size = new System.Drawing.Size(81, 13);
            this.LblDateDeDebut.TabIndex = 34;
            this.LblDateDeDebut.Text = "Date de début :";
            // 
            // dateTimePickerHeureFin
            // 
            this.dateTimePickerHeureFin.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePickerHeureFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerHeureFin.Location = new System.Drawing.Point(112, 165);
            this.dateTimePickerHeureFin.Name = "dateTimePickerHeureFin";
            this.dateTimePickerHeureFin.Size = new System.Drawing.Size(184, 20);
            this.dateTimePickerHeureFin.TabIndex = 33;
            // 
            // dateTimePickerHeureDebut
            // 
            this.dateTimePickerHeureDebut.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePickerHeureDebut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerHeureDebut.Location = new System.Drawing.Point(112, 129);
            this.dateTimePickerHeureDebut.Name = "dateTimePickerHeureDebut";
            this.dateTimePickerHeureDebut.Size = new System.Drawing.Size(184, 20);
            this.dateTimePickerHeureDebut.TabIndex = 32;
            // 
            // TxtBoxNumeroVacation
            // 
            this.TxtBoxNumeroVacation.Location = new System.Drawing.Point(110, 94);
            this.TxtBoxNumeroVacation.Name = "TxtBoxNumeroVacation";
            this.TxtBoxNumeroVacation.Size = new System.Drawing.Size(186, 20);
            this.TxtBoxNumeroVacation.TabIndex = 31;
            // 
            // LblNumeroVacation
            // 
            this.LblNumeroVacation.AutoSize = true;
            this.LblNumeroVacation.Location = new System.Drawing.Point(54, 97);
            this.LblNumeroVacation.Name = "LblNumeroVacation";
            this.LblNumeroVacation.Size = new System.Drawing.Size(50, 13);
            this.LblNumeroVacation.TabIndex = 30;
            this.LblNumeroVacation.Text = "Numéro :";
            // 
            // LblAtelierVacation
            // 
            this.LblAtelierVacation.AutoSize = true;
            this.LblAtelierVacation.Location = new System.Drawing.Point(9, 59);
            this.LblAtelierVacation.Name = "LblAtelierVacation";
            this.LblAtelierVacation.Size = new System.Drawing.Size(42, 13);
            this.LblAtelierVacation.TabIndex = 29;
            this.LblAtelierVacation.Text = "Atelier :";
            // 
            // CmbAtelierVacation
            // 
            this.CmbAtelierVacation.FormattingEnabled = true;
            this.CmbAtelierVacation.Location = new System.Drawing.Point(57, 56);
            this.CmbAtelierVacation.Name = "CmbAtelierVacation";
            this.CmbAtelierVacation.Size = new System.Drawing.Size(280, 21);
            this.CmbAtelierVacation.TabIndex = 7;
            // 
            // GrpBThemeAjout
            // 
            this.GrpBThemeAjout.Controls.Add(this.CmbAtelierTheme);
            this.GrpBThemeAjout.Controls.Add(this.TxtBoxLibelleTheme);
            this.GrpBThemeAjout.Controls.Add(this.TxtBoxNumeroTheme);
            this.GrpBThemeAjout.Controls.Add(this.LblLibelleTheme);
            this.GrpBThemeAjout.Controls.Add(this.LblNumeroTheme);
            this.GrpBThemeAjout.Controls.Add(this.LblAtelierTheme);
            this.GrpBThemeAjout.Location = new System.Drawing.Point(287, 175);
            this.GrpBThemeAjout.Name = "GrpBThemeAjout";
            this.GrpBThemeAjout.Size = new System.Drawing.Size(364, 117);
            this.GrpBThemeAjout.TabIndex = 2;
            this.GrpBThemeAjout.TabStop = false;
            this.GrpBThemeAjout.Text = "Thème :";
            this.GrpBThemeAjout.Visible = false;
            // 
            // CmbAtelierTheme
            // 
            this.CmbAtelierTheme.FormattingEnabled = true;
            this.CmbAtelierTheme.Location = new System.Drawing.Point(57, 19);
            this.CmbAtelierTheme.Name = "CmbAtelierTheme";
            this.CmbAtelierTheme.Size = new System.Drawing.Size(280, 21);
            this.CmbAtelierTheme.TabIndex = 4;
            // 
            // TxtBoxLibelleTheme
            // 
            this.TxtBoxLibelleTheme.Location = new System.Drawing.Point(78, 81);
            this.TxtBoxLibelleTheme.Name = "TxtBoxLibelleTheme";
            this.TxtBoxLibelleTheme.Size = new System.Drawing.Size(218, 20);
            this.TxtBoxLibelleTheme.TabIndex = 6;
            // 
            // TxtBoxNumeroTheme
            // 
            this.TxtBoxNumeroTheme.Location = new System.Drawing.Point(78, 55);
            this.TxtBoxNumeroTheme.Name = "TxtBoxNumeroTheme";
            this.TxtBoxNumeroTheme.Size = new System.Drawing.Size(218, 20);
            this.TxtBoxNumeroTheme.TabIndex = 5;
            // 
            // LblLibelleTheme
            // 
            this.LblLibelleTheme.AutoSize = true;
            this.LblLibelleTheme.Location = new System.Drawing.Point(29, 84);
            this.LblLibelleTheme.Name = "LblLibelleTheme";
            this.LblLibelleTheme.Size = new System.Drawing.Size(43, 13);
            this.LblLibelleTheme.TabIndex = 0;
            this.LblLibelleTheme.Text = "Libellé :";
            // 
            // LblNumeroTheme
            // 
            this.LblNumeroTheme.AutoSize = true;
            this.LblNumeroTheme.Location = new System.Drawing.Point(22, 58);
            this.LblNumeroTheme.Name = "LblNumeroTheme";
            this.LblNumeroTheme.Size = new System.Drawing.Size(50, 13);
            this.LblNumeroTheme.TabIndex = 0;
            this.LblNumeroTheme.Text = "Numéro :";
            // 
            // LblAtelierTheme
            // 
            this.LblAtelierTheme.AutoSize = true;
            this.LblAtelierTheme.Location = new System.Drawing.Point(9, 22);
            this.LblAtelierTheme.Name = "LblAtelierTheme";
            this.LblAtelierTheme.Size = new System.Drawing.Size(42, 13);
            this.LblAtelierTheme.TabIndex = 0;
            this.LblAtelierTheme.Text = "Atelier :";
            // 
            // GrpBAtelierAjout
            // 
            this.GrpBAtelierAjout.Controls.Add(this.TxtBoxLibelleAtelier);
            this.GrpBAtelierAjout.Controls.Add(this.NmUDPlacesMaxi);
            this.GrpBAtelierAjout.Controls.Add(this.LblNbPlacesMaxi);
            this.GrpBAtelierAjout.Controls.Add(this.LblLibelleAtelier);
            this.GrpBAtelierAjout.Location = new System.Drawing.Point(287, 76);
            this.GrpBAtelierAjout.Name = "GrpBAtelierAjout";
            this.GrpBAtelierAjout.Size = new System.Drawing.Size(364, 93);
            this.GrpBAtelierAjout.TabIndex = 1;
            this.GrpBAtelierAjout.TabStop = false;
            this.GrpBAtelierAjout.Text = "Atelier :";
            // 
            // TxtBoxLibelleAtelier
            // 
            this.TxtBoxLibelleAtelier.Location = new System.Drawing.Point(98, 30);
            this.TxtBoxLibelleAtelier.Name = "TxtBoxLibelleAtelier";
            this.TxtBoxLibelleAtelier.Size = new System.Drawing.Size(198, 20);
            this.TxtBoxLibelleAtelier.TabIndex = 2;
            // 
            // NmUDPlacesMaxi
            // 
            this.NmUDPlacesMaxi.Location = new System.Drawing.Point(98, 56);
            this.NmUDPlacesMaxi.Name = "NmUDPlacesMaxi";
            this.NmUDPlacesMaxi.Size = new System.Drawing.Size(100, 20);
            this.NmUDPlacesMaxi.TabIndex = 3;
            // 
            // LblNbPlacesMaxi
            // 
            this.LblNbPlacesMaxi.AutoSize = true;
            this.LblNbPlacesMaxi.Location = new System.Drawing.Point(22, 58);
            this.LblNbPlacesMaxi.Name = "LblNbPlacesMaxi";
            this.LblNbPlacesMaxi.Size = new System.Drawing.Size(70, 13);
            this.LblNbPlacesMaxi.TabIndex = 2;
            this.LblNbPlacesMaxi.Text = "Places Maxi :";
            // 
            // LblLibelleAtelier
            // 
            this.LblLibelleAtelier.AutoSize = true;
            this.LblLibelleAtelier.Location = new System.Drawing.Point(49, 33);
            this.LblLibelleAtelier.Name = "LblLibelleAtelier";
            this.LblLibelleAtelier.Size = new System.Drawing.Size(43, 13);
            this.LblLibelleAtelier.TabIndex = 1;
            this.LblLibelleAtelier.Text = "Libellé :";
            // 
            // GrpBChoixAjout
            // 
            this.GrpBChoixAjout.Controls.Add(this.RdbVacationAjout);
            this.GrpBChoixAjout.Controls.Add(this.RdbThemeAjout);
            this.GrpBChoixAjout.Controls.Add(this.RdbAtelierAjout);
            this.GrpBChoixAjout.Location = new System.Drawing.Point(10, 6);
            this.GrpBChoixAjout.Name = "GrpBChoixAjout";
            this.GrpBChoixAjout.Size = new System.Drawing.Size(940, 64);
            this.GrpBChoixAjout.TabIndex = 0;
            this.GrpBChoixAjout.TabStop = false;
            this.GrpBChoixAjout.Text = "Choisir un objet :";
            // 
            // RdbVacationAjout
            // 
            this.RdbVacationAjout.AutoSize = true;
            this.RdbVacationAjout.Location = new System.Drawing.Point(299, 30);
            this.RdbVacationAjout.Name = "RdbVacationAjout";
            this.RdbVacationAjout.Size = new System.Drawing.Size(67, 17);
            this.RdbVacationAjout.TabIndex = 0;
            this.RdbVacationAjout.TabStop = true;
            this.RdbVacationAjout.Text = "Vacation";
            this.RdbVacationAjout.UseVisualStyleBackColor = true;
            this.RdbVacationAjout.CheckedChanged += new System.EventHandler(this.RadioButtonVacation_CheckedChanged);
            // 
            // RdbThemeAjout
            // 
            this.RdbThemeAjout.AutoSize = true;
            this.RdbThemeAjout.Location = new System.Drawing.Point(173, 30);
            this.RdbThemeAjout.Name = "RdbThemeAjout";
            this.RdbThemeAjout.Size = new System.Drawing.Size(58, 17);
            this.RdbThemeAjout.TabIndex = 0;
            this.RdbThemeAjout.TabStop = true;
            this.RdbThemeAjout.Text = "Thème";
            this.RdbThemeAjout.UseVisualStyleBackColor = true;
            this.RdbThemeAjout.CheckedChanged += new System.EventHandler(this.RadioButtonTheme_CheckedChanged);
            // 
            // RdbAtelierAjout
            // 
            this.RdbAtelierAjout.AutoSize = true;
            this.RdbAtelierAjout.Checked = true;
            this.RdbAtelierAjout.Location = new System.Drawing.Point(55, 30);
            this.RdbAtelierAjout.Name = "RdbAtelierAjout";
            this.RdbAtelierAjout.Size = new System.Drawing.Size(54, 17);
            this.RdbAtelierAjout.TabIndex = 0;
            this.RdbAtelierAjout.TabStop = true;
            this.RdbAtelierAjout.Text = "Atelier";
            this.RdbAtelierAjout.UseVisualStyleBackColor = true;
            this.RdbAtelierAjout.CheckedChanged += new System.EventHandler(this.RdbAtelierAjout_CheckedChanged);
            // 
            // TabModifier
            // 
            this.TabModifier.Controls.Add(this.GrpBVacationModifier);
            this.TabModifier.Controls.Add(this.groupBox1);
            this.TabModifier.Controls.Add(this.BtnEnregistrerModifier);
            this.TabModifier.Controls.Add(this.GrpBThemeModifier);
            this.TabModifier.Controls.Add(this.GrpBAtelierModifier);
            this.TabModifier.Location = new System.Drawing.Point(4, 22);
            this.TabModifier.Name = "TabModifier";
            this.TabModifier.Padding = new System.Windows.Forms.Padding(3);
            this.TabModifier.Size = new System.Drawing.Size(956, 579);
            this.TabModifier.TabIndex = 2;
            this.TabModifier.Text = "Modifier ";
            this.TabModifier.UseVisualStyleBackColor = true;
            // 
            // GrpBVacationModifier
            // 
            this.GrpBVacationModifier.Controls.Add(this.label20);
            this.GrpBVacationModifier.Controls.Add(this.label21);
            this.GrpBVacationModifier.Controls.Add(this.dateTimePickerHeureFinModifier);
            this.GrpBVacationModifier.Controls.Add(this.dateTimePickerHeureDebutModifier);
            this.GrpBVacationModifier.Controls.Add(this.LblChoixVacation);
            this.GrpBVacationModifier.Controls.Add(this.comboBox4);
            this.GrpBVacationModifier.Location = new System.Drawing.Point(277, 368);
            this.GrpBVacationModifier.Name = "GrpBVacationModifier";
            this.GrpBVacationModifier.Size = new System.Drawing.Size(387, 148);
            this.GrpBVacationModifier.TabIndex = 11;
            this.GrpBVacationModifier.TabStop = false;
            this.GrpBVacationModifier.Text = "Vacation :";
            this.GrpBVacationModifier.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(89, 108);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 35;
            this.label20.Text = "Date de fin :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(73, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Date de début :";
            // 
            // dateTimePickerHeureFinModifier
            // 
            this.dateTimePickerHeureFinModifier.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePickerHeureFinModifier.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerHeureFinModifier.Location = new System.Drawing.Point(160, 102);
            this.dateTimePickerHeureFinModifier.Name = "dateTimePickerHeureFinModifier";
            this.dateTimePickerHeureFinModifier.Size = new System.Drawing.Size(184, 20);
            this.dateTimePickerHeureFinModifier.TabIndex = 33;
            // 
            // dateTimePickerHeureDebutModifier
            // 
            this.dateTimePickerHeureDebutModifier.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePickerHeureDebutModifier.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerHeureDebutModifier.Location = new System.Drawing.Point(160, 66);
            this.dateTimePickerHeureDebutModifier.Name = "dateTimePickerHeureDebutModifier";
            this.dateTimePickerHeureDebutModifier.Size = new System.Drawing.Size(184, 20);
            this.dateTimePickerHeureDebutModifier.TabIndex = 32;
            // 
            // LblChoixVacation
            // 
            this.LblChoixVacation.AutoSize = true;
            this.LblChoixVacation.Location = new System.Drawing.Point(11, 22);
            this.LblChoixVacation.Name = "LblChoixVacation";
            this.LblChoixVacation.Size = new System.Drawing.Size(109, 13);
            this.LblChoixVacation.TabIndex = 29;
            this.LblChoixVacation.Text = "Choisir une vacation :";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(126, 19);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(218, 21);
            this.comboBox4.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RdbVacationModifier);
            this.groupBox1.Controls.Add(this.RdbThemeModifier);
            this.groupBox1.Controls.Add(this.RdbAtelierModifier);
            this.groupBox1.Location = new System.Drawing.Point(10, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(940, 64);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choisir un objet :";
            // 
            // RdbVacationModifier
            // 
            this.RdbVacationModifier.AutoSize = true;
            this.RdbVacationModifier.Location = new System.Drawing.Point(299, 30);
            this.RdbVacationModifier.Name = "RdbVacationModifier";
            this.RdbVacationModifier.Size = new System.Drawing.Size(67, 17);
            this.RdbVacationModifier.TabIndex = 0;
            this.RdbVacationModifier.TabStop = true;
            this.RdbVacationModifier.Text = "Vacation";
            this.RdbVacationModifier.UseVisualStyleBackColor = true;
            // 
            // RdbThemeModifier
            // 
            this.RdbThemeModifier.AutoSize = true;
            this.RdbThemeModifier.Location = new System.Drawing.Point(173, 30);
            this.RdbThemeModifier.Name = "RdbThemeModifier";
            this.RdbThemeModifier.Size = new System.Drawing.Size(58, 17);
            this.RdbThemeModifier.TabIndex = 0;
            this.RdbThemeModifier.TabStop = true;
            this.RdbThemeModifier.Text = "Thème";
            this.RdbThemeModifier.UseVisualStyleBackColor = true;
            this.RdbThemeModifier.CheckedChanged += new System.EventHandler(this.RdbThemeModifier_CheckedChanged);
            // 
            // RdbAtelierModifier
            // 
            this.RdbAtelierModifier.AutoSize = true;
            this.RdbAtelierModifier.Checked = true;
            this.RdbAtelierModifier.Location = new System.Drawing.Point(55, 30);
            this.RdbAtelierModifier.Name = "RdbAtelierModifier";
            this.RdbAtelierModifier.Size = new System.Drawing.Size(54, 17);
            this.RdbAtelierModifier.TabIndex = 0;
            this.RdbAtelierModifier.TabStop = true;
            this.RdbAtelierModifier.Text = "Atelier";
            this.RdbAtelierModifier.UseVisualStyleBackColor = true;
            // 
            // BtnEnregistrerModifier
            // 
            this.BtnEnregistrerModifier.Location = new System.Drawing.Point(427, 532);
            this.BtnEnregistrerModifier.Name = "BtnEnregistrerModifier";
            this.BtnEnregistrerModifier.Size = new System.Drawing.Size(75, 23);
            this.BtnEnregistrerModifier.TabIndex = 9;
            this.BtnEnregistrerModifier.Text = "Enregistrer";
            this.BtnEnregistrerModifier.UseVisualStyleBackColor = true;
            // 
            // GrpBThemeModifier
            // 
            this.GrpBThemeModifier.Controls.Add(this.LblChoixTheme);
            this.GrpBThemeModifier.Controls.Add(this.comboBox3);
            this.GrpBThemeModifier.Controls.Add(this.CmbAtelierThemeModifier);
            this.GrpBThemeModifier.Controls.Add(this.TxtBoxLibelleThemeModifier);
            this.GrpBThemeModifier.Controls.Add(this.TxtBoxNumeroThemeModifier);
            this.GrpBThemeModifier.Controls.Add(this.label14);
            this.GrpBThemeModifier.Controls.Add(this.label15);
            this.GrpBThemeModifier.Controls.Add(this.label16);
            this.GrpBThemeModifier.Location = new System.Drawing.Point(277, 206);
            this.GrpBThemeModifier.Name = "GrpBThemeModifier";
            this.GrpBThemeModifier.Size = new System.Drawing.Size(387, 156);
            this.GrpBThemeModifier.TabIndex = 7;
            this.GrpBThemeModifier.TabStop = false;
            this.GrpBThemeModifier.Text = "Thème :";
            this.GrpBThemeModifier.Visible = false;
            // 
            // LblChoixTheme
            // 
            this.LblChoixTheme.AutoSize = true;
            this.LblChoixTheme.Location = new System.Drawing.Point(30, 22);
            this.LblChoixTheme.Name = "LblChoixTheme";
            this.LblChoixTheme.Size = new System.Drawing.Size(91, 13);
            this.LblChoixTheme.TabIndex = 8;
            this.LblChoixTheme.Text = "Choisir un thème :";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(126, 19);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(218, 21);
            this.comboBox3.TabIndex = 7;
            // 
            // CmbAtelierThemeModifier
            // 
            this.CmbAtelierThemeModifier.FormattingEnabled = true;
            this.CmbAtelierThemeModifier.Location = new System.Drawing.Point(126, 66);
            this.CmbAtelierThemeModifier.Name = "CmbAtelierThemeModifier";
            this.CmbAtelierThemeModifier.Size = new System.Drawing.Size(218, 21);
            this.CmbAtelierThemeModifier.TabIndex = 4;
            // 
            // TxtBoxLibelleThemeModifier
            // 
            this.TxtBoxLibelleThemeModifier.Location = new System.Drawing.Point(126, 119);
            this.TxtBoxLibelleThemeModifier.Name = "TxtBoxLibelleThemeModifier";
            this.TxtBoxLibelleThemeModifier.Size = new System.Drawing.Size(218, 20);
            this.TxtBoxLibelleThemeModifier.TabIndex = 6;
            // 
            // TxtBoxNumeroThemeModifier
            // 
            this.TxtBoxNumeroThemeModifier.Location = new System.Drawing.Point(126, 93);
            this.TxtBoxNumeroThemeModifier.Name = "TxtBoxNumeroThemeModifier";
            this.TxtBoxNumeroThemeModifier.Size = new System.Drawing.Size(218, 20);
            this.TxtBoxNumeroThemeModifier.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(76, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Libellé :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(69, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Numéro :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(77, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Atelier :";
            // 
            // GrpBAtelierModifier
            // 
            this.GrpBAtelierModifier.Controls.Add(this.LblChoixAtelier);
            this.GrpBAtelierModifier.Controls.Add(this.CmbChoixAtelier);
            this.GrpBAtelierModifier.Controls.Add(this.TxtBoxLibelleAtelierModifier);
            this.GrpBAtelierModifier.Controls.Add(this.NmUDPlacesMaxiModifier);
            this.GrpBAtelierModifier.Controls.Add(this.label18);
            this.GrpBAtelierModifier.Controls.Add(this.label19);
            this.GrpBAtelierModifier.Location = new System.Drawing.Point(277, 76);
            this.GrpBAtelierModifier.Name = "GrpBAtelierModifier";
            this.GrpBAtelierModifier.Size = new System.Drawing.Size(387, 124);
            this.GrpBAtelierModifier.TabIndex = 6;
            this.GrpBAtelierModifier.TabStop = false;
            this.GrpBAtelierModifier.Text = "Atelier :";
            // 
            // LblChoixAtelier
            // 
            this.LblChoixAtelier.AutoSize = true;
            this.LblChoixAtelier.Location = new System.Drawing.Point(30, 22);
            this.LblChoixAtelier.Name = "LblChoixAtelier";
            this.LblChoixAtelier.Size = new System.Drawing.Size(90, 13);
            this.LblChoixAtelier.TabIndex = 6;
            this.LblChoixAtelier.Text = "Choisir un atelier :";
            // 
            // CmbChoixAtelier
            // 
            this.CmbChoixAtelier.FormattingEnabled = true;
            this.CmbChoixAtelier.Location = new System.Drawing.Point(126, 19);
            this.CmbChoixAtelier.Name = "CmbChoixAtelier";
            this.CmbChoixAtelier.Size = new System.Drawing.Size(218, 21);
            this.CmbChoixAtelier.TabIndex = 5;
            // 
            // TxtBoxLibelleAtelierModifier
            // 
            this.TxtBoxLibelleAtelierModifier.Location = new System.Drawing.Point(126, 61);
            this.TxtBoxLibelleAtelierModifier.Name = "TxtBoxLibelleAtelierModifier";
            this.TxtBoxLibelleAtelierModifier.Size = new System.Drawing.Size(218, 20);
            this.TxtBoxLibelleAtelierModifier.TabIndex = 2;
            // 
            // NmUDPlacesMaxiModifier
            // 
            this.NmUDPlacesMaxiModifier.Location = new System.Drawing.Point(126, 87);
            this.NmUDPlacesMaxiModifier.Name = "NmUDPlacesMaxiModifier";
            this.NmUDPlacesMaxiModifier.Size = new System.Drawing.Size(99, 20);
            this.NmUDPlacesMaxiModifier.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(50, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Places Maxi :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(77, 64);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Libellé :";
            // 
            // FrmPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 605);
            this.Controls.Add(this.TabPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrincipale";
            this.Load += new System.EventHandler(this.FrmPrincipale_Load);
            this.TabInscription.ResumeLayout(false);
            this.GrpBenevole.ResumeLayout(false);
            this.GrpBenevole.PerformLayout();
            this.GrpTypeParticipant.ResumeLayout(false);
            this.GrpTypeParticipant.PerformLayout();
            this.GrpIdentite.ResumeLayout(false);
            this.GrpIdentite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicAffiche)).EndInit();
            this.GrpIntervenant.ResumeLayout(false);
            this.GrpIntervenant.PerformLayout();
            this.GrpNuiteIntervenant.ResumeLayout(false);
            this.GrpNuiteIntervenant.PerformLayout();
            this.TabPrincipal.ResumeLayout(false);
            this.TabCreer.ResumeLayout(false);
            this.GrpBVacationAjout.ResumeLayout(false);
            this.GrpBVacationAjout.PerformLayout();
            this.GrpBThemeAjout.ResumeLayout(false);
            this.GrpBThemeAjout.PerformLayout();
            this.GrpBAtelierAjout.ResumeLayout(false);
            this.GrpBAtelierAjout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NmUDPlacesMaxi)).EndInit();
            this.GrpBChoixAjout.ResumeLayout(false);
            this.GrpBChoixAjout.PerformLayout();
            this.TabModifier.ResumeLayout(false);
            this.GrpBVacationModifier.ResumeLayout(false);
            this.GrpBVacationModifier.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GrpBThemeModifier.ResumeLayout(false);
            this.GrpBThemeModifier.PerformLayout();
            this.GrpBAtelierModifier.ResumeLayout(false);
            this.GrpBAtelierModifier.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NmUDPlacesMaxiModifier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage TabInscription;
        private System.Windows.Forms.GroupBox GrpBenevole;
        private System.Windows.Forms.Button BtnEnregistreBenevole;
        private System.Windows.Forms.Panel PanelDispoBenevole;
        private System.Windows.Forms.MaskedTextBox TxtLicenceBenevole;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox TxtDateNaissance;
        private System.Windows.Forms.Button CmdQuitter;
        private System.Windows.Forms.GroupBox GrpTypeParticipant;
        private System.Windows.Forms.RadioButton RadLicencie;
        private System.Windows.Forms.RadioButton RadBenevole;
        private System.Windows.Forms.RadioButton RadIntervenant;
        private System.Windows.Forms.GroupBox GrpIdentite;
        private System.Windows.Forms.TextBox TxtMail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txtTel;
        private System.Windows.Forms.MaskedTextBox TxtCp;
        private System.Windows.Forms.TextBox TxtVille;
        private System.Windows.Forms.TextBox TxtAdr2;
        private System.Windows.Forms.TextBox TxtAdr1;
        private System.Windows.Forms.TextBox TxtPrenom;
        private System.Windows.Forms.TextBox TxtNom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox PicAffiche;
        private System.Windows.Forms.GroupBox GrpIntervenant;
        private System.Windows.Forms.Button BtnEnregistrerIntervenant;
        private System.Windows.Forms.GroupBox GrpNuiteIntervenant;
        private System.Windows.Forms.Panel PanNuiteIntervenant;
        private System.Windows.Forms.RadioButton RdbNuiteIntervenantNon;
        private System.Windows.Forms.RadioButton RdbNuiteIntervenantOui;
        private System.Windows.Forms.Panel PanFonctionIntervenant;
        private System.Windows.Forms.ComboBox CmbAtelierIntervenant;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabControl TabPrincipal;
        private System.Windows.Forms.TabPage TabCreer;
        private System.Windows.Forms.GroupBox GrpBAtelierAjout;
        private System.Windows.Forms.GroupBox GrpBChoixAjout;
        private System.Windows.Forms.RadioButton RdbAtelierAjout;
        private System.Windows.Forms.RadioButton RdbVacationAjout;
        private System.Windows.Forms.RadioButton RdbThemeAjout;
        private System.Windows.Forms.GroupBox GrpBVacationAjout;
        private System.Windows.Forms.GroupBox GrpBThemeAjout;
        private System.Windows.Forms.TextBox TxtBoxLibelleTheme;
        private System.Windows.Forms.TextBox TxtBoxNumeroTheme;
        private System.Windows.Forms.Label LblLibelleTheme;
        private System.Windows.Forms.Label LblNumeroTheme;
        private System.Windows.Forms.Label LblAtelierTheme;
        private System.Windows.Forms.TextBox TxtBoxLibelleAtelier;
        private System.Windows.Forms.NumericUpDown NmUDPlacesMaxi;
        private System.Windows.Forms.Label LblNbPlacesMaxi;
        private System.Windows.Forms.Label LblLibelleAtelier;
        private System.Windows.Forms.ComboBox CmbAtelierTheme;
        private System.Windows.Forms.ComboBox CmbAtelierVacation;
        private System.Windows.Forms.TextBox TxtBoxNumeroVacation;
        private System.Windows.Forms.Label LblNumeroVacation;
        private System.Windows.Forms.Label LblAtelierVacation;
        private System.Windows.Forms.Label LblDateDeFin;
        private System.Windows.Forms.Label LblDateDeDebut;
        private System.Windows.Forms.DateTimePicker dateTimePickerHeureFin;
        private System.Windows.Forms.DateTimePicker dateTimePickerHeureDebut;
        private System.Windows.Forms.Button BtnEnregistrerAjout;
        private System.Windows.Forms.TabPage TabModifier;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button BtnEnregistrerModifier;
        private System.Windows.Forms.GroupBox GrpBThemeModifier;
        private System.Windows.Forms.Label LblChoixTheme;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox CmbAtelierThemeModifier;
        private System.Windows.Forms.TextBox TxtBoxLibelleThemeModifier;
        private System.Windows.Forms.TextBox TxtBoxNumeroThemeModifier;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox GrpBAtelierModifier;
        private System.Windows.Forms.Label LblChoixAtelier;
        private System.Windows.Forms.ComboBox CmbChoixAtelier;
        private System.Windows.Forms.TextBox TxtBoxLibelleAtelierModifier;
        private System.Windows.Forms.NumericUpDown NmUDPlacesMaxiModifier;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox GrpBVacationModifier;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dateTimePickerHeureFinModifier;
        private System.Windows.Forms.DateTimePicker dateTimePickerHeureDebutModifier;
        private System.Windows.Forms.Label LblChoixVacation;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RdbVacationModifier;
        private System.Windows.Forms.RadioButton RdbThemeModifier;
        private System.Windows.Forms.RadioButton RdbAtelierModifier;
    }
}